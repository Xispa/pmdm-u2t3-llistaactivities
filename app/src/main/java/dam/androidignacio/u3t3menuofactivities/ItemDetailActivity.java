package dam.androidignacio.u3t3menuofactivities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ItemDetailActivity extends AppCompatActivity {

    ImageView ivLogo;
    TextView tvName;
    TextView tvVersion;
    TextView tvApi;
    TextView tvYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        setUI();
    }

    void setUI() {
        // TODO Ex1
        // TODO Ex2

        int image = getIntent().getIntExtra("image", 0);
        ivLogo = findViewById(R.id.ivLogoDetail);
        ivLogo.setImageResource(image);

        String name = getIntent().getStringExtra("name");
        tvName = findViewById(R.id.tvNameDetail);
        tvName.setText(name);

        String version = getIntent().getStringExtra("version");
        tvVersion = findViewById(R.id.tvVersionDetail);
        tvVersion.setText("Version: " + version);

        String api = getIntent().getStringExtra("api");
        tvApi = findViewById(R.id.tvAPIDetail);
        tvApi.setText("Version: " + api);

        String year = getIntent().getStringExtra("year");
        tvYear = findViewById(R.id.tvYearDetail);
        tvYear.setText(year);

        ivLogo.setOnClickListener(v ->  {
            String url = getIntent().getStringExtra("url");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        });

    }
}
