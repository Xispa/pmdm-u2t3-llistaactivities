package dam.androidignacio.u3t3menuofactivities;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidignacio.u3t3menuofactivities.model.Item;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<Item> myDataSet;
    private OnItemClickListener listener;

    // Constructor to set list data and listener for onItemClick
    MyAdapter(ArrayList<Item> myDataSet, OnItemClickListener listener) {
        this.myDataSet = myDataSet;
        // set listener for onItemClick
        this.listener = listener;
    }

    // Create new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // TODO Ex2
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_recycler_item, parent, false);

        return new MyViewHolder(cv);
    }

    // Replaces the data content of a viewholder: Layout Manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        // bind viewHolder with data at: position
        // set also listener to viewHolder
        viewHolder.bind(myDataSet.get(position), listener);
    }

    // Return the size of dataSet: Layout Manager calls this method
    @Override
    public int getItemCount() {
        return myDataSet.size();
    }

    //**********************************************************************************************

    public interface OnItemClickListener {
        void onItemClickListener(Item itemSelected);
    }

    // TODO Ex2
    // Class for each list item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView ivLogo;
        TextView tvName;
        TextView tvVersion;
        TextView tvAPI;

        public MyViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
            this.ivLogo = cardView.findViewById(R.id.ivLogo);
            this.tvName = cardView.findViewById(R.id.tvName);
            this.tvVersion = cardView.findViewById(R.id.tvVersion);
            this.tvAPI = cardView.findViewById(R.id.tvAPI);
        }

        // sets viewHolder with data
        public void bind(Item itemSelected, OnItemClickListener listener) {
            this.tvName.setText(itemSelected.getName());
            this.tvAPI.setText("API: " + itemSelected.getApi());
            this.tvVersion.setText("Version: " + itemSelected.getVersion());
            this.ivLogo.setImageResource(itemSelected.getImage());

            // call listener when click on TextView
            this.cardView.setOnClickListener(v -> listener.onItemClickListener(itemSelected));
        }

    }

}
