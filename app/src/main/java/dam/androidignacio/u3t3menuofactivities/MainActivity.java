package dam.androidignacio.u3t3menuofactivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import java.util.ArrayList;

import dam.androidignacio.u3t3menuofactivities.model.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private Button btnAdd, btnDeleteAll;

    // TODO Ex2
    private ArrayList<Item> myDataset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
        addItem();
        removeItem();
    }

    private void setUI() {

        myDataset = initialize();
        btnAdd = findViewById(R.id.btnAdd);
        btnDeleteAll = findViewById(R.id.btnDeleteAll);
        recyclerView = findViewById(R.id.recyclerViewActivities);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // set recylerView with a linear layout manager
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        // set an adapter to recyclerView
        mAdapter = new MyAdapter(myDataset, this);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClickListener(Item itemSelected) {
        // TODO Ex1
        // TODO Ex2
        Intent intent = new Intent(this, ItemDetailActivity.class);
        intent.putExtra("image", itemSelected.getImage());
        intent.putExtra("name", itemSelected.getName());
        intent.putExtra("version", itemSelected.getVersion());
        intent.putExtra("api", itemSelected.getApi());
        intent.putExtra("year", itemSelected.getYear());
        intent.putExtra("url", itemSelected.getWikiURL());
        startActivity(intent);
    }

    //TODO Ex3
    private void addItem() {
        btnAdd.setOnClickListener(v -> {
            Item item = new Item(R.drawable.a_12, "12.0", "Android 12", "31", "2021", "https://es.wikipedia.org/wiki/Android_12");
            myDataset.add(item);
            mAdapter.notifyItemInserted(myDataset.size() - 1);
            mAdapter.notifyDataSetChanged();
            recyclerView.smoothScrollToPosition(mAdapter.getItemCount());
        });
    }

    // TODO Ex3
    private void removeItem() {
        btnDeleteAll.setOnClickListener(v -> {
            if (myDataset.size() > 0) {
                myDataset.remove(mAdapter.getItemCount() - 1);
                mAdapter.notifyItemRemoved(mAdapter.getItemCount() - 1);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    // TODO Ex2
    private ArrayList<Item> initialize() {
        ArrayList<Item> dataSet = new ArrayList<>();
        dataSet.add(new Item(R.drawable.jellybean, "4.1", "Jelly Bean", "16", "2012", "https://es.wikipedia.org/wiki/Android_Jelly_Bean"));
        dataSet.add(new Item(R.drawable.kitkat, "4.4", "Kit Kat", "19", "2013", "https://es.wikipedia.org/wiki/Android_KitKat"));
        dataSet.add(new Item(R.drawable.lollipop, "5.0", "Lollipop", "21", "2014", "https://es.wikipedia.org/wiki/Android_Lollipop"));
        dataSet.add(new Item(R.drawable.marshmallow, "6.0", "Marshmallow", "23", "2015", "https://es.wikipedia.org/wiki/Android_Marshmallow"));
        dataSet.add(new Item(R.drawable.nougat, "7.0", "Nougat", "24", "2016", "https://es.wikipedia.org/wiki/Android_Nougat"));
        dataSet.add(new Item(R.drawable.oreo, "8.0", "Oreo", "26", "2017", "https://es.wikipedia.org/wiki/Android_Oreo"));
        dataSet.add(new Item(R.drawable.pie, "9.0", "Pie", "28", "2018", "https://es.wikipedia.org/wiki/Android_Pie"));
        dataSet.add(new Item(R.drawable.a_10, "10.0", "Android 10", "29", "2019", "https://es.wikipedia.org/wiki/Android_10"));
        dataSet.add(new Item(R.drawable.a_11, "11.0", "Android 11", "30", "2020", "https://es.wikipedia.org/wiki/Android_11"));
        return dataSet;
    }

}