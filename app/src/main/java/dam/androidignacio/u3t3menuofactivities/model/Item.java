package dam.androidignacio.u3t3menuofactivities.model;

public class Item {

    private int image;
    private String version;
    private String name;
    private String api;
    private String year;
    private String wikiURL;

    public Item(int image, String version, String name, String api, String year, String wikiURL) {
        this.image = image;
        this.version = version;
        this.name = name;
        this.api = api;
        this.year = year;
        this.wikiURL = wikiURL;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getWikiURL() {
        return wikiURL;
    }

    public void setWikiURL(String wikiURL) {
        this.wikiURL = wikiURL;
    }
}
